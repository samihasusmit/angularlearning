import { Injectable } from '@angular/core';
import { Icing } from './icings';
import { ICINGS } from './mock-icings';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IcingService {
  private icingsUrl = 'api/icings';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  // getIcings(): Icing[] {
  //   return ICINGS;
  // }

  // getIcings(): Observable<Icing[]> {
  //   const icings = of(ICINGS);
  //   return icings;
  // }

  // getIcings(): Observable<Icing[]> {
  //   const icings = of(ICINGS);
  //   this.messageService.add('IcingService: Fetched icings');
  //   return icings;
  // }

  /** GET icings from the server */
  getIcings(): Observable<Icing[]> {
    return this.http.get<Icing[]>(this.icingsUrl)
    .pipe(
      tap(_ => this.log('Fetched icings')),
      catchError(this.handleError<Icing[]>('getIcings', []))
    );
  }

  // getIcing(id: number): Observable<Icing> {
  //   // For now, assume that a icing with the specified `id` always exists.
  //   // Error handling will be added in the next step of the tutorial.
  //   const icing = ICINGS.find(h => h.id === id)!;
  //   this.messageService.add(`IcingService: Fetched icing id=${id}`);
  //   return of(icing);
  // }

  /** GET icing by id. Will 404 if id not found */
  getIcing(id: number): Observable<Icing> {
    const url = `${this.icingsUrl}/${id}`;
    return this.http.get<Icing>(url).pipe(
      tap(_ => this.log(`Fetched icing id=${id}`)),
      catchError(this.handleError<Icing>(`getIcing id=${id}`))
    );
  }

  /** PUT: update the icing on the server */
  updateIcing(icing: Icing): Observable<any> {
    return this.http.put(this.icingsUrl, icing, this.httpOptions).pipe(
      tap(_ => this.log(`Updated icing id=${icing.id}`)),
      catchError(this.handleError<any>('updateIcing'))
    );
  }

  /** POST: add a new icing to the server */
  addIcing(icing: Icing): Observable<Icing> {
    return this.http.post<Icing>(this.icingsUrl, icing, this.httpOptions).pipe(
      tap((newIcing: Icing) => this.log(`Added icing w/ id=${newIcing.id}`)),
      catchError(this.handleError<Icing>('addIcing'))
    );
  }

  /** DELETE: delete the icing from the server */
  deleteIcing(id: number): Observable<Icing> {
    const url = `${this.icingsUrl}/${id}`;

    return this.http.delete<Icing>(url, this.httpOptions).pipe(
      tap(_ => this.log(`Deleted icing id=${id}`)),
      catchError(this.handleError<Icing>('deleteIcing'))
    );
  }

  /* GET icings whose name contains search term */
  searchIcings(term: string): Observable<Icing[]> {
    if (!term.trim()) {
      // if not search term, return empty icing array.
      return of([]);
    }
    return this.http.get<Icing[]>(`${this.icingsUrl}/?name=${term}`).pipe(
      tap(x => x.length ?
        this.log(`Found icings matching "${term}"`) :
        this.log(`No icings matching "${term}"`)),
      catchError(this.handleError<Icing[]>('searchIcings', []))
    );
  }

  /** Log a IcingService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`IcingService: ${message}`);
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
