import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IcingsComponent } from './icings/icings.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { IcingDetailComponent } from './icing-detail/icing-detail.component';

const routes: Routes = [
  { path: 'icings', component: IcingsComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'detail/:id', component: IcingDetailComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }