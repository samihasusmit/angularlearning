import { Component, OnInit } from '@angular/core';
import { Icing } from '../icings';
import { IcingService } from '../icing.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  icings: Icing[] = [];

  constructor(private icingService: IcingService) { }

  ngOnInit(): void {
    this.getIcings();
  }

  getIcings(): void {
    this.icingService.getIcings()
      .subscribe(icings => this.icings = icings.slice(1, 5));
  }
}