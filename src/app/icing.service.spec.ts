import { TestBed } from '@angular/core/testing';

import { IcingService } from './icing.service';

describe('IcingService', () => {
  let service: IcingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IcingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
