import { Icing } from './icings';

export const ICINGS: Icing[] = [
  { id: 11, name: 'Chocolate Dip' },
  { id: 12, name: 'Rainbow Flakes' },
  { id: 13, name: 'Cookies & Creme' },
  { id: 14, name: 'Cereals' },
  { id: 15, name: 'Vanilla Flakes' },
  { id: 16, name: 'Choco Drops' },
  { id: 17, name: 'Sugar Bits' }
];