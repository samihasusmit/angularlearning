import { Component, OnInit } from '@angular/core';
import { Icing } from '../icings';
import { ICINGS } from '../mock-icings';
import { IcingService } from '../icing.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-icings',
  templateUrl: './icings.component.html',
  styleUrls: ['./icings.component.css']
})

export class IcingsComponent implements OnInit {
  icings: Icing[] = [];

  constructor(private icingService: IcingService) { }

  ngOnInit(): void {
    this.getIcings();
  }

  getIcings(): void {
    this.icingService.getIcings()
    .subscribe(icings => this.icings = icings);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.icingService.addIcing({ name } as Icing)
      .subscribe(icing => {
        this.icings.push(icing);
      });
  }

  delete(icing: Icing): void {
    this.icings = this.icings.filter(h => h !== icing);
    this.icingService.deleteIcing(icing.id).subscribe();
  }


}


//Notes
//export class IcingsComponent implements OnInit {

  // icings: Icing = {
  //   id: 1,
  //   name: 'chocolate dip'
  // };

  // icings = ICINGS;

  // icings: Icing[] = [];


  // constructor(private icingService: IcingService) {}

  // getIcings(): void {
  //   this.icings = this.icingService.getIcings();
  // }

  //Part 2
  // getIcings(): void {
  //   this.icingService.getIcings()
  //       .subscribe(icings => this.icings = icings);
  // }

  // ngOnInit(): void {
  //   this.getIcings();
  // }

  // selectedIcing?: Icing;
  // onSelect(icing: Icing): void {
  //   this.selectedIcing = icing;
  // }

  //Part 3
  // selectedIcing?: Icing;

  // icings: Icing[] = [];

  // constructor(private icingService: IcingService, private messageService: MessageService) { }

  // ngOnInit(): void {
  //   this.getIcinges();
  // }

  // onSelect(icing: Icing): void {
  //   this.selectedIcing = icing;
  //   this.messageService.add(`IcingesComponent: Selected icing id=${icing.id}`);
  // }

  // getIcinges(): void {
  //   this.icingService.getIcings()
  //       .subscribe(icings => this.icings = icings);
  // }



//}


