import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcingsComponent } from './icings.component';

describe('IcingsComponent', () => {
  let component: IcingsComponent;
  let fixture: ComponentFixture<IcingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
