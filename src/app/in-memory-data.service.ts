import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Icing } from './icings';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const icings = [
      { id: 11, name: 'Chocolate Dip' },
      { id: 12, name: 'Rainbow Flakes' },
      { id: 13, name: 'Cookies & Creme' },
      { id: 14, name: 'Cereals' },
      { id: 15, name: 'Vanilla Flakes' },
      { id: 16, name: 'Choco Drops' },
      { id: 17, name: 'Sugar Bits' }
    ];
    return {icings};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  genId(icings: Icing[]): number {
    return icings.length > 0 ? Math.max(...icings.map(icing => icing.id)) + 1 : 11;
  }
}