import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcingDetailComponent } from './icing-detail.component';

describe('IcingDetailComponent', () => {
  let component: IcingDetailComponent;
  let fixture: ComponentFixture<IcingDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcingDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcingDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
