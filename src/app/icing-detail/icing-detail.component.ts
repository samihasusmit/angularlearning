import { Component, OnInit, Input } from '@angular/core';
import { Icing } from '../icings';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { IcingService } from '../icing.service';

@Component({
  selector: 'app-icing-detail',
  templateUrl: './icing-detail.component.html',
  styleUrls: ['./icing-detail.component.css']
})
export class IcingDetailComponent implements OnInit {

  @Input() icing?: Icing;

  constructor(
    private route: ActivatedRoute,
    private icingService: IcingService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getIcing();
  }
  
  getIcing(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.icingService.getIcing(id)
      .subscribe(icing => this.icing = icing);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (this.icing) {
      this.icingService.updateIcing(this.icing)
        .subscribe(() => this.goBack());
    }
  }

}
