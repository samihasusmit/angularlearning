import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IcingSearchComponent } from './icing-search.component';

describe('IcingSearchComponent', () => {
  let component: IcingSearchComponent;
  let fixture: ComponentFixture<IcingSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IcingSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IcingSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
