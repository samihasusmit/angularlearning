import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Icing } from '../icings';
import { IcingService } from '../icing.service';

@Component({
  selector: 'app-icing-search',
  templateUrl: './icing-search.component.html',
  styleUrls: [ './icing-search.component.css' ]
})
export class IcingSearchComponent implements OnInit {
  icings$!: Observable<Icing[]>;
  private searchTerms = new Subject<string>();

  constructor(private icingService: IcingService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.icings$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.icingService.searchIcings(term)),
    );
  }
}